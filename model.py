from datetime import datetime
import logging as log

from PySide2.QtCore import Qt, Signal
from PySide2.QtGui import QStandardItemModel, QStandardItem, QBrush


class Upload:
    def __init__(self, filename):
        self.filename = filename
        self.completed = False
        self.failed = False
        self.progress = 0
        self.index = None
        self.last_update = datetime.now()
        self.last_sent = 0


class UploadModel(QStandardItemModel):
    """Model to handle controllers"""

    FILENAME = 0
    PROGRESS = 1
    TIME = 2

    cancelRequested = Signal(str)

    def __init__(self, parent):
        super().__init__(0, 3, parent)
        self.setHorizontalHeaderLabels(["Filename", "Progress", "Remaining time"])

        # We use two dictionaries for faster access in exchange of
        # of a little more memory. Network requests come with the filename
        # as key, while UI requests use the index
        self.map_name = {}
        self.map_index = {}

    def uploadError(self, filename, reason):
        """The upload of filename errored"""
        upload = self.map_name[filename]
        upload.failed = True
        # Update both items to color both columns
        topleft = self.index(upload.index, self.FILENAME)
        bottomright = self.index(upload.index, self.TIME)
        self.dataChanged.emit(topleft, bottomright, [Qt.BackgroundRole])

    def uploadFinished(self, filename):
        """The upload of filename finished"""
        upload = self.map_name[filename]

        if upload.failed:
            # It was already handled by the error callback
            return

        upload.completed = True
        # Update both items to color both columns
        topleft = self.index(upload.index, self.FILENAME)
        bottomright = self.index(upload.index, self.TIME)
        self.dataChanged.emit(topleft, bottomright, [Qt.BackgroundRole])

    def uploadStarted(self, filename):
        fileItem = QStandardItem(filename)
        progressItem = QStandardItem()
        progressItem.setData(0, Qt.UserRole + 1000)
        timeItem = QStandardItem("-")

        self.appendRow([fileItem, progressItem, timeItem])
        # Index into the pct column
        index = self.index(self.rowCount() - 1, 1)
        upload = Upload(filename)
        self.map_name[filename] = upload
        self.map_index[index.row()] = upload

        upload.index = index.row()

    def uploadProgress(self, filename, sent, total):
        if total == 0 or sent == 0:
            return
        upload = self.map_name[filename]

        now = datetime.now()
        elapsed_this_chunk = now - upload.last_update
        chunk_size = sent - upload.last_sent
        remaining_size = total - sent
        chunks_remaining = remaining_size / chunk_size
        time_left = chunks_remaining * elapsed_this_chunk

        self.setData(
            self.index(self.map_name[filename].index, self.TIME), f"{time_left}"
        )

        pct = sent * 100 / total
        log.debug(f"Setting data of {filename} to {pct}")
        self.setData(
            self.index(self.map_name[filename].index, self.PROGRESS),
            pct,
            Qt.UserRole + 1000,
        )

    def data(self, index, role):
        upload = self.map_index[index.row()]
        if role == Qt.BackgroundRole:
            if upload.completed:
                color = Qt.green
                return QBrush(color)
            elif upload.failed:
                color = Qt.red
                return QBrush(color)

        return super().data(index, role)

    def cancel(self, index):
        upload = self.map_index[index.row()]
        log.debug(f"Requested to cancel upload of {upload.filename}")

        if upload.failed or upload.completed:
            log.debug(
                f"Upload of {upload.filename} already finished. Nothing to cancel"
            )
            return

        self.cancelRequested.emit(upload.filename)
