"""Main ui-related classes of PyUploader"""

import logging as log

from PySide2.QtCore import Signal, QSize, Qt, QModelIndex
from PySide2.QtGui import QIcon, QKeySequence
from PySide2.QtNetwork import QNetworkReply
from PySide2.QtWidgets import (
    QAction,
    QApplication,
    QErrorMessage,
    QFileDialog,
    QHeaderView,
    QMainWindow,
    QMenu,
    QStyle,
    QStyledItemDelegate,
    QStyleOptionProgressBar,
    QSystemTrayIcon,
    QTableView,
)

from network import NetworkCredentials
from dialogs import LoginDialog

# Progress from https://stackoverflow.com/q/54285057/347889
class ProgressDelegate(QStyledItemDelegate):
    def paint(self, painter, options, index):
        progress = index.data(Qt.UserRole + 1000)
        opt = QStyleOptionProgressBar()
        opt.rect = options.rect
        opt.minimum = 0
        opt.maximum = 100
        opt.progress = progress
        opt.text = "{:.1f}%".format(progress)
        opt.textVisible = True
        QApplication.style().drawControl(QStyle.CE_ProgressBar, opt, painter)


class PyUploaderWindow(QMainWindow):
    """Main window of PyUploader."""

    loginCredentialsChanged = Signal(NetworkCredentials)
    uploadRequested = Signal(str)
    uploadCanceled = Signal(QModelIndex)

    def __init__(self, app, model):
        """Basic initialization"""
        super().__init__()

        self.app = app

        self.setMinimumSize(640, 480)

        self.create_actions()
        self.create_menus()
        self.create_toolbar()

        self.setup_tray_icon()

        delegate = ProgressDelegate(self)
        self.view = QTableView(self)
        self.view.setContextMenuPolicy(Qt.CustomContextMenu)
        self.view.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.view.customContextMenuRequested.connect(self.table_menu)
        self.view.setItemDelegateForColumn(1, delegate)

        self.view.setModel(model)
        self.setCentralWidget(self.view)

        self.show()

        if self.tray_icon is not None:
            self.app.setQuitOnLastWindowClosed(False)

    def closeEvent(self, evt):
        """Window manager requested to close window."""
        log.debug("called")
        self.hide()
        evt.ignore()

    def hideEvent(self, evt):
        """Application requested to hide this window."""
        log.debug("called")
        super().hideEvent(evt)
        self.update_tray_menu(False)

    def showEvent(self, evt):
        """Window will be made visible again"""
        log.debug("called")
        super().showEvent(evt)
        self.update_tray_menu(True)

    def create_actions(self):
        """Create the actions and their shortcuts, connecting to slots."""

        self.close_action = QAction("&Close", self)
        self.close_action.setShortcuts(QKeySequence.Close)
        self.close_action.setStatusTip("Closes to tray")
        self.close_icon = self.create_icon(QStyle.SP_TitleBarMinButton)
        self.close_action.setIcon(self.close_icon)
        self.close_action.triggered.connect(self.close_triggered)

        self.quit_action = QAction("&Quit", self)
        self.quit_action.setShortcuts(QKeySequence.Quit)
        self.quit_action.setStatusTip("Exits the application")
        self.quit_icon = self.create_icon(QStyle.SP_TitleBarCloseButton)
        self.quit_action.setIcon(self.quit_icon)
        self.quit_action.triggered.connect(self.quit_triggered)

        self.restore_action = QAction("&Restore", self)
        self.restore_action.setStatusTip("Restore the application window")
        self.restore_icon = self.create_icon(QStyle.SP_TitleBarNormalButton)
        self.restore_action.setIcon(self.restore_icon)
        self.restore_action.triggered.connect(self.restore_triggered)

        self.minimize_action = QAction("&Minimize", self)
        self.minimize_action.setStatusTip("Minimize the app to the tray bar.")
        self.minimize_icon = self.create_icon(QStyle.SP_TitleBarMinButton)
        self.minimize_action.setIcon(self.minimize_icon)
        self.minimize_action.triggered.connect(self.minimize_triggered)

        self.login_action = QAction("&Configure login", self)
        self.login_action.setStatusTip("Configure login information.")
        self.login_icon = self.create_icon(QStyle.SP_DriveNetIcon)
        self.login_action.setIcon(self.login_icon)
        self.login_action.triggered.connect(self.login_triggered)

        self.upload_action = QAction("&Upload file", self)
        self.upload_action.setStatusTip("Select file and upload.")
        self.upload_action.triggered.connect(self.upload_triggered)
        self.upload_action.setEnabled(False)
        self.upload_icon = self.create_icon(QStyle.SP_FileIcon)
        self.upload_action.setIcon(self.upload_icon)
        self.app.loginConfigured.connect(lambda: self.upload_action.setEnabled(True))

    def quit_triggered(self):
        """User asked to quit the application.

        This should check if we have any pending upload before quitting."""
        log.debug("Quit triggered. Exiting app main loop")
        self.app.quit()

    def close_triggered(self):
        """User asked to close the window."""
        if self.tray_icon is not None:
            log.debug("Close triggered. Have tray icon, minimizing.")
            self.hide()
        else:
            log.debug("Close triggered. No tray icon, closing.")
            self.close()

    def restore_triggered(self):
        """Make the window visible again"""
        log.debug("Restore triggered. Showing window again.")
        self.show()

    def minimize_triggered(self):
        """Minimize the app to the tray bar"""
        log.debug("Minimize triggered. Hiding window.")
        self.hide()

    def login_triggered(self):
        """Configure login information"""
        log.debug("Configure login triggered.")
        dialog = LoginDialog(self, self.app.credentials)
        if dialog.exec_():
            url = dialog.server_url
            username = dialog.username
            password = dialog.password
            log.debug(
                f"Got url {url}, username {username} and password {password} from dialog."
            )
            self.loginCredentialsChanged.emit(
                NetworkCredentials(url, username, password)
            )

    def upload_triggered(self):
        """Select file and ask to upload"""
        log.debug("Upload select triggered.")
        filename = QFileDialog.getOpenFileName(self, "Upload file")
        log.debug(f"User selected {filename}")
        self.uploadRequested.emit(filename[0])

    def create_menus(self):
        file_menu = self.menuBar().addMenu("&File")
        file_menu.addAction(self.login_action)
        file_menu.addAction(self.upload_action)
        file_menu.addSeparator()
        file_menu.addAction(self.close_action)
        file_menu.addAction(self.quit_action)

    def create_toolbar(self):
        toolbar = self.addToolBar("File")
        toolbar.addAction(self.login_action)
        toolbar.addAction(self.upload_action)

    def table_menu(self, point):
        index = self.view.indexAt(point)

        menu = QMenu(self)
        action = QAction("Cancel", self)
        menu.addAction(action)
        action.triggered.connect(lambda: self.uploadCanceled.emit(index))

        menu.popup(self.view.viewport().mapToGlobal(point))

    def create_icon(self, style_pixmap):
        icon = QIcon()
        size = QSize(32, 32)
        icon.addPixmap(self.app.style().standardIcon(style_pixmap).pixmap(size))

        return icon

    def setup_tray_icon(self):
        """Creates the tray icon and attaches the event handlers"""

        if not QSystemTrayIcon.isSystemTrayAvailable:
            self.tray_icon = None
            return

        icon = self.create_icon(QStyle.SP_ComputerIcon)
        self.tray_icon = QSystemTrayIcon(icon, parent=None)
        self.tray_icon.activated.connect(self.tray_activated)
        self.add_tray_menu()
        self.tray_icon.show()

    def add_tray_menu(self):
        """Setups the correct tray menu"""

        log.debug(f"Adding tray menu.")

        menu = QMenu("PyUploader Client", self)

        if self.isVisible():
            menu.addAction(self.minimize_action)
        else:
            menu.addAction(self.restore_action)

        menu.addSeparator()

        menu.addAction(self.quit_action)

        self.tray_icon.setContextMenu(menu)

    def update_tray_menu(self, is_window_visible):

        log.debug(f"Window visible: {is_window_visible}")

        menu = self.tray_icon.contextMenu()

        if is_window_visible:
            log.debug("Replacing restore action with minimize")
            menu.insertAction(self.restore_action, self.minimize_action)
            menu.removeAction(self.restore_action)
        else:
            log.debug("Replacing minimize action with restore")
            menu.insertAction(self.minimize_action, self.restore_action)
            menu.removeAction(self.minimize_action)

    def tray_activated(self, reason):
        """The user clicked on the tray icon"""
        log.debug(f"Tray activated with reason {reason}")
        if reason == QSystemTrayIcon.Trigger:
            if self.isVisible():
                self.hide()
            else:
                self.show()

    def uploadError(self, filename, reason):
        if reason == QNetworkReply.OperationCanceledError:
            return

        errorDialog = QErrorMessage(self)
        errorDialog.showMessage(f"Upload of {filename} failed. Error: {reason}")
