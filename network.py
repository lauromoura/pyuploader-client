import os
import base64
import logging as log
from functools import partial
from collections import namedtuple

from PySide2.QtCore import QObject, QUrl, QFile, Signal, QIODevice
from PySide2.QtNetwork import (
    QHttpMultiPart,
    QHttpPart,
    QNetworkAccessManager,
    QNetworkRequest,
    QNetworkReply,
)

NetworkCredentials = namedtuple("NetworkCredentials", ["server", "user", "password"])


class PyUploaderNetworkManager(QObject):
    """Handles network task for PyUploader"""

    loginConfigured = Signal()

    uploadStarted = Signal(str)
    uploadError = Signal(str, QNetworkReply.NetworkError)
    uploadFinished = Signal(str)
    uploadProgress = Signal(str, int, int)

    def __init__(self, app):
        """Basic initialization."""
        super().__init__(app)
        self.app = app
        self.qnam = QNetworkAccessManager()
        self.url = None
        self.auth_info = None
        self.requests = {}

    def login_changed(self, credentials):
        """Called when the login credentials should change"""
        log.debug("Login changed. updating credentials")
        self.server = credentials.server
        self.user = credentials.user
        self.password = credentials.password
        self.check_credentials()

    def check_credentials(self):
        """Checks if the credentials are filled."""
        if all([self.server, self.user, self.password]):
            # This could allow enabling the upload button right at the start of the app.
            self.url = QUrl(self.server + "/pyuploader/upload")

            encoded_credentials = base64.encodebytes(
                bytes(self.user + ":" + self.password, "ascii")
            )
            self.auth_info = encoded_credentials[:-1]

            self.loginConfigured.emit()

    def upload(self, full_filename):
        """Requests to upload the given file."""
        log.debug(f"Will upload file {full_filename}")

        if self.url is None:
            raise ValueError("Can't upload without login information.")

        filename = os.path.split(full_filename)[-1]
        if filename in self.requests:
            raise ValueError("Can't upload a file with the same name as a current one.")

        log.debug(f"Target url is {self.url}")

        multipart = QHttpMultiPart(QHttpMultiPart.FormDataType)

        part = QHttpPart()
        # The server config requires this `filename` field
        part.setHeader(
            QNetworkRequest.ContentDispositionHeader,
            f'form-data; name="file"; filename="{os.path.split(filename)[-1]}"',
        )

        file = QFile(full_filename)
        if not file.open(QIODevice.ReadOnly):
            raise IOError("Failed to open file...")
        part.setBodyDevice(file)
        # File can't be collected until the part finishes transmitting
        file.setParent(multipart)

        multipart.append(part)

        request = QNetworkRequest()
        request.setUrl(self.url)
        request.setRawHeader(b"x-test", b"true")
        request.setRawHeader(b"Authorization", b"Basic " + self.auth_info)

        reply = self.qnam.post(request, multipart)
        reply.error.connect(partial(self.reply_error, filename))
        reply.finished.connect(partial(self.reply_finished, filename))
        reply.uploadProgress.connect(partial(self.reply_progress, filename))
        # Keep the multipart alive as long as the reply
        multipart.setParent(reply)

        self.requests[filename] = reply

        self.uploadStarted.emit(filename)

    def reply_error(self, filename, reason):
        """Reports an upload has failed."""
        log.error(f"Error uploading {filename}. Reason: {reason}")
        self.uploadError.emit(filename, reason)

    def reply_finished(self, filename):
        """Reports an upload has finished"""
        log.debug(f"Finished uploading {filename}")
        del self.requests[filename]
        self.uploadFinished.emit(filename)

    def reply_progress(self, filename, sent, total):
        """Reports some upload has progressed"""
        log.debug(f"Bytes sent of {filename}: {sent}, total of {total}")
        self.uploadProgress.emit(filename, sent, total)

    def cancel(self, filename):
        log.debug(f"Cancelling upload of {filename}")
        request = self.requests[filename]
        request.abort()

    @property
    def credentials(self):
        return NetworkCredentials(self.server, self.user, self.password)

    @property
    def server(self):
        return self.app.get_setting("network/server") or ""

    @server.setter
    def server(self, url):
        self.app.set_setting("network/server", url)

    @property
    def user(self):
        return self.app.get_setting("network/user") or ""

    @user.setter
    def user(self, username):
        self.app.set_setting("network/user", username)

    @property
    def password(self):
        return self.app.get_setting("network/password") or ""

    @password.setter
    def password(self, passwd):
        # FIXME Storing password in plain text because YOLO :D
        self.app.set_setting("network/password", passwd)
