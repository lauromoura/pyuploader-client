"""Custom dialogs for PyUploader"""

from PySide2.QtCore import Qt
from PySide2.QtWidgets import (
    QDialog,
    QLabel,
    QLineEdit,
    QVBoxLayout,
    QHBoxLayout,
    QDialogButtonBox,
    QPushButton,
)


class LoginDialog(QDialog):
    """Basic login configuration dialog."""

    def __init__(self, parent, credentials):
        super().__init__(parent)

        # Inputs
        server_label = QLabel("Server URL:port")
        self.server_edit = QLineEdit()
        self.server_edit.setText(credentials.server)
        server_label.setBuddy(self.server_edit)

        user_label = QLabel("Username")
        self.user_edit = QLineEdit()
        self.user_edit.setText(credentials.user)
        user_label.setBuddy(self.user_edit)

        password_label = QLabel("Password")
        self.password_edit = QLineEdit()
        self.password_edit.setEchoMode(QLineEdit.Password)
        self.password_edit.setText(credentials.password)
        password_label.setBuddy(self.password_edit)

        self.server_edit.textChanged.connect(self.validate)
        self.user_edit.textChanged.connect(self.validate)
        self.password_edit.textChanged.connect(self.validate)

        self.main_layout = QVBoxLayout()

        # Input layouts
        server_layout = QHBoxLayout()
        server_layout.addWidget(server_label)
        server_layout.addWidget(self.server_edit)
        self.main_layout.addLayout(server_layout)

        user_layout = QHBoxLayout()
        user_layout.addWidget(user_label)
        user_layout.addWidget(self.user_edit)
        self.main_layout.addLayout(user_layout)

        password_layout = QHBoxLayout()
        password_layout.addWidget(password_label)
        password_layout.addWidget(self.password_edit)
        self.main_layout.addLayout(password_layout)

        # Buttons
        button_box = QDialogButtonBox(Qt.Horizontal)
        self.ok_button = QPushButton("Ok")
        self.ok_button.setEnabled(
            all([credentials.server, credentials.user, credentials.password])
        )
        cancel_button = QPushButton("Cancel")

        button_box.addButton(cancel_button, QDialogButtonBox.ActionRole)
        button_box.addButton(self.ok_button, QDialogButtonBox.ActionRole)

        self.ok_button.clicked.connect(self.accept)
        cancel_button.clicked.connect(self.reject)

        self.main_layout.addWidget(button_box)

        self.setLayout(self.main_layout)

        self.setModal(True)

    @property
    def server_url(self):
        """Gets the server url the user input"""
        return self.server_edit.text()

    @property
    def username(self):
        """Gets the username the user input"""
        return self.user_edit.text()

    @property
    def password(self):
        """Gets the password the user input"""
        return self.password_edit.text()

    def validate(self):
        """Basic validation to enable the ok button."""
        self.ok_button.setEnabled(
            all(
                [
                    self.server_edit.text(),
                    self.user_edit.text(),
                    self.password_edit.text(),
                ]
            )
        )
