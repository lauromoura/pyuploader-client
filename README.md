PyUploader - Desktop client
===========================

This is a simple application that connects to the PyUploader server and uploads
files from the desktop.

It uses PySide2/Qt5 for the UI.

To execute, just run the pyuploader-client script.

When closing, the app will minimize to the traybar. To quit, use `CTLR+Q` or `File->Quit` in the menu.

To cancel, right click on the filename and select cancel.

For the server, just add the `http://<IP>:<PORT>` string in the login configuration dialog.

TODO
----

* [x] - Minimize app to the traybar
* [x] - Login into pyuploader server (Storing the user/passwd to be passed to
        the upload requests, not a keep-alive connection)
* [x] - Add support for the file upload
* [x] - View current uploads progress.
* [x] - Error Reporting
* [x] - Cancel an ongoing upload.
